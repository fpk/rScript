# rScript
Current version **1.6**

### Install in firefox
    https://addons.mozilla.org/ru/firefox/addon/rscript/

### How to build addon
    npm install
    make build

### How to create XPI
    Enter **make xpi_build**
