import {handleJsOnPage, handleJsRequest, handleMessage, handleOnHeaders, handleLocalhostAccess} from './handle'
import {extractHostname} from '../utils/util'
import {loadData, allowList} from './store'
import {DEFAULT_ALLOW, ALLOW_ICON, BLOCK_ICON, IMUTABLES_LIST} from './defaults'

setInterval(function() {
    const querying = browser.tabs.query({active:true});
	querying.then(function(tab) {
        const url = tab[0].url;
        let iconPath = BLOCK_ICON;

		if(allowList.includes(extractHostname(url)) || IMUTABLES_LIST.includes(url))
			iconPath = ALLOW_ICON;

		browser.browserAction.setIcon({path: iconPath});
	});
}, 300);

function init() {
    loadData();

	if(allowList === undefined)
		allowList = DEFAULT_ALLOW;

    browser.webRequest.onBeforeRequest.addListener(
        handleLocalhostAccess,
        {urls:["<all_urls>"]},
		["blocking"]
	);

	browser.webRequest.onBeforeRequest.addListener(
        handleJsRequest,
        {urls:["<all_urls>"], types: ["script", "main_frame"]},
		["blocking"]
	);

	browser.webRequest.onBeforeRequest.addListener(
        handleJsOnPage,
        {urls: ["<all_urls>"], types: ["main_frame"]},
		["blocking"]
	);

    browser.webRequest.onHeadersReceived.addListener(
      handleOnHeaders,
      {urls: ["<all_urls>"], types: ["main_frame"]},
      ["blocking", "responseHeaders"]
    )

	browser.runtime.onMessage.addListener(handleMessage);
}

init();
