import {storeData, allowList, updateUrlOn, urlOn, setAllowList} from './store'
import {DENY_TAGS, DENY_SUBNETS} from './defaults'
import {extractHostname, isIPHostname} from '../utils/util'

export function handleLocalhostAccess(requestDetails) {
    const url = extractHostname(requestDetails.url)
    if(requestDetails.originUrl && isIPHostname(url)) {
        for(const subnet of DENY_SUBNETS) {
            if(url.startsWith(subnet)) {
                console.log(`Trying access to ${url}. Reject`)
                return {cancel: true}
            }
        }
    }
    return {}
}

export function handleJsRequest(requestDetails) {
    if(requestDetails.type === "main_frame") {
        urlOn[requestDetails.tabId] = {};
        return {}
    }

    console.log(`Request url: (${requestDetails.url}) adding in (${requestDetails.documentUrl})`);
    for(let domain of allowList) {
        let domain_url = requestDetails.url;
        let hostname = extractHostname(domain_url);

        updateUrlOn(requestDetails.tabId, hostname);
        if(domain === hostname) {
            console.log(`Domain ${domain} passed`);
            return {};
        }
    }
    return {cancel: true}
}

let urlHeaders = {}

function parseCharset(header) {
    const pos = header.search(/charset/i)
    if(pos === -1) {
        return ""
    }
    const encoding = header.substr(pos + "charset=".length)
    return encoding
}

function parseContentType(header) {
    const pos = header.search(/;/i)
    let ret;
    if(pos === -1) {
        ret = header
    } else {
        ret = header.substr(0, pos)
    }
    return ret
}

export function handleOnHeaders(details) {
    const url = details.url
    const hostname = extractHostname(url)
    if (allowList.includes(hostname))
        return {}

    let contentTypeIndex = null
    let needSendCharset = false
    let fileType;

    for(var headerIndex in details.responseHeaders) {
        const header = details.responseHeaders[headerIndex]
        if(header.name.search(/content\-type/i) != -1) {
            let encoding = parseCharset(header.value)
            fileType = parseContentType(header.value)
            if(encoding.length === 0) {
                console.log("Encoding is null, set to UTF-8")
                encoding = "utf-8"
            }

            urlHeaders[url] = encoding
            contentTypeIndex = headerIndex

            if(fileType.search(/text/gi) !== -1) {
                needSendCharset = true
            }
        }
    }

    if(needSendCharset) {
        details.responseHeaders[contentTypeIndex].value = fileType + "; charset=utf-8"
        return {responseHeaders: details.responseHeaders}
    }
}

function concatResponses(responseParts) {
    if(responseParts.length === 0)
        return new Uint8Array(0)

    const responseUIntList = responseParts.map(array => new Uint8Array(array))
    const responseUIntSize = responseParts.reduce((a, b) => {
        return {byteLength: a.byteLength + b.byteLength}
    }).byteLength
    const concatedArray = new Uint8Array(responseUIntSize)

    let offset = 0
    for(var responsePart of responseUIntList) {
        concatedArray.set(responsePart, offset)
        offset += responsePart.byteLength
    }
    return concatedArray
}

function getCharset(pageUrl) {
    let charset = urlHeaders[pageUrl]
    if(!charset) {
        console.log(`Encoding for (${pageUrl}) is NULL. Set to UTF-8`)
        charset = "UTF-8"
    }
    return charset
}

function unsetCharsetFor(pageUrl) {
    if(getCharset(pageUrl))
        delete urlHeaders[pageUrl]
}

function constructDecEnc(charset) {
    let decoder;
    let encoder;
    try {
        decoder = new TextDecoder(charset);
        encoder = new TextEncoder("utf-8");
    } catch(e) {
        console.log(`Encoding "${charset}": ${e}`)
        decoder = new TextDecoder("utf-8");
        encoder = new TextEncoder("utf-8");
    }
    return {decoder, encoder}
}

export function handleJsOnPage(details) {
    console.log("event -> handleJsOnPage");

    const url = details.url;
    const hostname = extractHostname(url);
    updateUrlOn(details.tabId, hostname);

    if (allowList.includes(hostname))
        return {};

    const filter = browser.webRequest.filterResponseData(details.requestId);
    let foundScript = false;
    let responseParts = []
    filter.onstop = event => {
        console.log("Loading finish. Local data")

        const charset = getCharset(url)
        const concatedArray = concatResponses(responseParts)
        const {decoder, encoder} = constructDecEnc(charset)

        let str = decoder.decode(concatedArray, {stream: true});
        for(const tag of DENY_TAGS) {
            if (str.search(tag.search) !== -1) {
                console.log(`Found ${tag.name} on page ${url}`);
                str = str.replace(tag.replace, "");
                foundScript = true;
            }
        }
        if(foundScript) {
            filter.write(encoder.encode(str));
        } else {
            filter.write(concatedArray)
            console.log("Nothing to change")
        }

        unsetCharsetFor(url)
        filter.disconnect();
    }

    filter.ondata = event => {
        responseParts.push(event.data)
    }
    return {};
}

export function handleMessage(request, sender, sendResponse) {
    console.log(`Type hanlde: ${request.type}`);

    if(request.type === "get_tab_on") {
        const tabId = request.tabId;
        sendResponse({response: urlOn[tabId]});
    }
    else if(request.type === "get_allowed") {
        sendResponse({response: allowList});
    }
    else if(request.type === "add_allow") {
        const allowUrl = request.allowUrl;
        console.log(`Allow ${allowUrl} url`);

        allowList.push(allowUrl);
        storeData()
    }
    else if(request.type === "remove_allow") {
        const allowUrl = request.allowUrl;
        console.log(`Remove ${allowUrl} url`);

        setAllowList(allowList.filter((e) => e !== allowUrl));
        storeData()
    }
}
