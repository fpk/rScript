export const BLOCK_ICON = "icons/deny.png";
export const ALLOW_ICON = "icons/allowed.png";
export const DEFAULT_ALLOW = ["about:config"];

export const IMUTABLES_LIST = ["about:newtab", "about:config", "about:debugging"];
export const DENY_TAGS = [
    {name: "scripts", search: /<script/gi, replace: /<script[\s\S]*?>[\s\S]*?<\/script>/gi},
    {name: "iframes", search: /<iframe/gi, replace: /<iframe[\s\S]*?>[\s\S]*?<\/iframe>/gi}
]

export const DENY_SUBNETS = ["192.168."]
