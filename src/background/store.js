import {DEFAULT_ALLOW} from './defaults'

export let allowList = [];
export let urlOn = {};

export function setAllowList(newList) {
    allowList = newList
}

export function updateUrlOn(docUrl, hostname) {
    if(!urlOn[docUrl])
        urlOn[docUrl] = {};
    urlOn[docUrl][hostname] = 1
}

function onLoadAllowList(items) {
    let loadedData = items.allowListData;
    if(loadedData === undefined) {
        allowList = DEFAULT_ALLOW;
        return;
    }
    allowList = items.allowListData
}

export function storeData() {
    console.log("Save:");
    console.log({allowListData: allowList});
    browser.storage.local.set({allowListData: allowList})
}

export function loadData() {
    let gettingItem = browser.storage.local.get();

    gettingItem.then(onLoadAllowList,
        function(error) {
            console.log(`Storage error: ${error}`)
        }
    );
}
