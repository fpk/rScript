let urls_div = document.querySelector("#urls");
let body_content = document.querySelector("html");

let querying = browser.tabs.query({active:true});
let haveAnyChange = false;

function showData(msg) {
	let [tabUrl, allowedUrls] = msg;

	for(let url in tabUrl.response) {
		let li = document.createElement("li");
		let ah = document.createElement("a");

		ah.innerHTML = `<span class='text'>Allow</span>
						<span class='url'>${url}</span>
						<span class='text'>permanently</span>`;

		ah.href = "#";
        ah.urlData = url;
		li.appendChild(ah);

		if(allowedUrls.response.includes(url)) {
			li.className = "selected"
		}
		urls_div.appendChild(li)
	}

    let allATag = document.querySelectorAll("a");
	allATag.forEach((elem) => {
		elem.addEventListener("click", function(event) {
			event.preventDefault();

            let urlData;
			let curTag = event.target;
			let tagName = curTag.tagName;
            let classList = curTag.parentNode.className;

			if(tagName === "SPAN") {
                curTag = curTag.parentNode;
                urlData = curTag.urlData;
                classList = curTag.parentNode.className;
			} else {
                urlData = curTag.urlData
			}

			if (classList === "selected") {
                curTag.parentNode.className = "";

				browser.runtime.sendMessage({
					type: "remove_allow",
					allowUrl: urlData
				});
			} else {
                curTag.parentNode.className = "selected";

				browser.runtime.sendMessage({
					type: "add_allow",
					allowUrl: urlData
				});
			}

			haveAnyChange = true
		});
	});
}

let currentTabId;


body_content.addEventListener("mouseleave", function() {
	if(haveAnyChange) {
		browser.tabs.reload(currentTabId);
		haveAnyChange = false;
		window.close();
	}
});

querying.then(function(tabInfo) {
	console.log(tabInfo);
	currentTabId = tabInfo[0].id;
	currentUrl = tabInfo[0].url;

	console.log("current tab " + currentTabId);

    let getTabPromise = browser.runtime.sendMessage({type: "get_tab_on", tabId: currentTabId});
	let getAllowedPromise = browser.runtime.sendMessage({type: "get_allowed"});

	Promise.all([getTabPromise, getAllowedPromise]).then(showData)
});
