export function extractHostname(url) {
    let domainUrl = url;
    let hostnameStart = domainUrl.indexOf("://");
    let hostnameEnd = domainUrl.substr(hostnameStart + 3).indexOf("/");
    let hostname;

    if (hostnameEnd === -1)
        hostnameEnd = domainUrl.lenght; // FIXME: wtf?

    hostname = domainUrl.substr(hostnameStart + 3, hostnameEnd);
    hostname = hostname.replace("www.", "");
    return hostname
}

export function isIPHostname(url) {
    return url.search(/([^.\d])/gi) === -1
}
