pack:
	npm run-script build

build: pack
	mkdir -p addon
	cp -r icons addon
	cp src/manifest.json addon/
	cp src/popup/popup_index.html addon/popup

xpi_build: build
	mkdir -p out
	cd addon && zip -r -FS ../out/rScript-extension.zip *