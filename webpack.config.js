const path = require("path");

module.exports = {
    entry: {
        background_scripts: "./src/background/background.js",
        popup: "./src/popup/script_popup.js"
    },
    output: {
        path: path.resolve(__dirname, "addon"),
        filename: "[name]/index.js"
    }
};